/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-04-03T08:15:57+02:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#include <chrono>
#include <ctime>
#include <exception>

#include <boost/filesystem.hpp>

#include <DIMData.h>
#include <NTOFLogging.hpp>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <easylogging++.h>

#include "Config.h"
#include "Env.hpp"
#include "RFMCli.hpp"
#include "RFMManager.hpp"
#include "test_helpers.hpp"
#include "test_helpers_rfm.hpp"

namespace bfs = boost::filesystem;
using namespace ntof::rfm;
using namespace ntof::log;
using ntof::dim::DIMData;

class TestRFMManagerExpiry : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestRFMManagerExpiry);
    CPPUNIT_TEST(expireRun);
    CPPUNIT_TEST_SUITE_END();

    std::unique_ptr<DimTestHelper> m_dim;
    bfs::path m_path;

public:
    void setUp() override
    {
        m_path = envInit("TestRFMManagerExpiry");
        genFiles(m_path);
        m_dim.reset(new DimTestHelper());
    }

    void tearDown() override
    {
        m_dim.reset();
        envDestroy(m_path);
    }

    void expireRun()
    {
        RFMManager mgr;
        mgr.initialize();
        mgr.fileDeleter()->setInterval(250);
        mgr.monitor()->setInterval(250);
        mgr.start();

        RFMCli cli;

        bool approve = false;
        DaqInfo::CrateIdList daqs;
        DaqInfo::fromString("1:2", daqs);
        EQ(true, cli.cmdStart(901234, daqs, "testEx", &approve));

        EQ(true, cli.cmdEvent(901234, 0, 0, 1024));
        EQ(true, cli.cmdEvent(901234, 1, 1, 1024));

        EQ(true, cli.cmdStop(901234));
        std::time_t now = std::time(nullptr);

        RunInfoWaiter waiter;
        DIMData info;
        EQ(true,
           waiter.wait(
               [&info](DIMData::List &data) {
                   LOG(ERROR) << data;
                   info = DIMData(0, data);
                   DIMData count = getData("901234.waitingApprovalCount", info);
                   return (count.getULongValue() == 1);
               },
               3000));
        LOG(INFO) << info;

        std::time_t expiry = getData("901234.expiryDate", info).getUIntValue();
        DBL_EQ(now + Config::instance().getDefaultExpiry(), expiry, 1);

        EQ(true, bfs::exists(m_path / "local" / "run901234"));
        EQ(true, bfs::exists(m_path / "mnt" / "ntofdaq-m111" / "run901234"));

        EQ(true, cli.setRunExpiry(901234, now - 10));
        EQ(true,
           waiter.wait([&info](DIMData::List &data) { return data.size() == 1; },
                       3000));
        EQ(false, bfs::exists(m_path / "local" / "run901234"));
        EQ(false, bfs::exists(m_path / "mnt" / "ntofdaq-m111" / "run901234"));
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestRFMManagerExpiry);
