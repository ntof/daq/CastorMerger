/*
 * RFMCommand.cpp
 *
 *  Created on: Jan 8, 2016
 *      Author: mdonze
 */

#include "RFMCommand.hpp"

#include <ctime>

#include <NTOFException.h>
#include <easylogging++.h>

#include "Config.h"
#include "RFMException.hpp"
#include "RFMFactory.hpp"
#include "RFMManager.hpp"
#include "data/DaqInfo.hpp"

using namespace ntof::rfm;

#define NTOF_THROW(msg, code) \
    throw ntof::NTOFException(msg, __FILE__, __LINE__, code)

RFMCommand::RFMCommand(RFMManager &mgr) :
    ntof::dim::DIMSuperCommand(Config::instance().getDimPrefix()),
    m_mgr(mgr)
{}

void RFMCommand::commandReceived(ntof::dim::DIMCmd &cmdData)
{
    pugi::xml_node parent = cmdData.getData();
    std::string cmd = parent.attribute("name").as_string("");

    LOG(DEBUG)
        << "Received command " << cmd << " with key " << cmdData.getKey();

    try
    {
        if (cmd == "event")
            cmdEvent(parent);
        else if (cmd == "runStop")
            cmdRunStop(parent);
        else if (cmd == "runStart")
            cmdRunStart(parent);
        else if (cmd == "fileIgnore")
            cmdFileIgnore(parent);
        else
        {
            NTOF_THROW("Invalid command : " + cmd, 2);
        }
        setOk(cmdData.getKey(), "OK");
    }
    catch (ntof::NTOFException &ex)
    {
        LOG(ERROR) << "Command failed: " << ex.what();
        setError(cmdData.getKey(), ex.getErrorCode(), ex.what());
    }
    catch (std::exception &ex)
    {
        LOG(ERROR) << "Command failed: " << ex.what();
        setError(cmdData.getKey(), 1, ex.what());
    }
    catch (...)
    {
        LOG(ERROR) << "Command failed: unknown error";
        setError(cmdData.getKey(), 1, "unknown error");
    }
}

void RFMCommand::cmdEvent(pugi::xml_node &node)
{
    // Event completed by EACS/DAQ
    RunInfo::RunNumber runNumber =
        node.attribute("runNumber").as_uint(RunInfo::InvalidRunNumber);
    if (runNumber == RunInfo::InvalidRunNumber)
        NTOF_THROW("Invalid runNumber", 1);

    int32_t timingEventNumber = node.attribute("timingEvent").as_int(-1);
    if (timingEventNumber < 0)
        NTOF_THROW("Invalid timingEvent: " + std::to_string(timingEventNumber),
                   1);

    int32_t seqEventNumber = node.attribute("validatedEvent").as_int(-1);
    if (seqEventNumber < 0)
        NTOF_THROW("Invalid validatedEvent: " + std::to_string(seqEventNumber),
                   1);

    uint64_t fileSize = node.attribute("fileSize").as_ullong(0);

    if (!m_mgr.addEvent(runNumber, seqEventNumber, timingEventNumber, fileSize))
        NTOF_THROW("Failed to add event", 1);
}

void RFMCommand::cmdRunStart(pugi::xml_node &node)
{
    RunInfo::RunNumber runNumber =
        node.attribute("runNumber").as_uint(RunInfo::InvalidRunNumber);
    if (runNumber == RunInfo::InvalidRunNumber)
        NTOF_THROW("Invalid runNumber", 1);

    const std::string experiment = node.attribute("experiment").as_string();
    pugi::xml_attribute approved = node.attribute("approved");
    pugi::xml_attribute standalone = node.attribute("standalone");

    DaqInfo::CrateIdList daqs;
    for (const pugi::xml_node &daq : node.children("daq"))
    {
        DaqInfo::CrateId crateId = daq.attribute("crateId").as_int(-1);
        if (crateId < 0)
            NTOF_THROW("Invalid daq element", 1);
        else if (!RFMFactory::instance().getDaqInfo(crateId))
            NTOF_THROW("Unknown daq: " + std::to_string(crateId), 1);

        daqs.insert(crateId);
    }

    if (daqs.empty())
        NTOF_THROW("No daqs associated with this run", 1);

    RunInfo::Shared run = RFMFactory::instance().createRun(
        runNumber, Config::instance().getExperimentalArea(), std::time(nullptr),
        daqs, experiment);

    if (!run)
        NTOF_THROW("Failed to create run", 1);

    if (approved && (approved.as_int() > 0))
        run->setApproved(true);
    else
        run->setExpiryDate(std::time(nullptr) +
                           Config::instance().getDefaultExpiry());

    if (standalone && (standalone.as_int() > 0))
        run->setStandalone();

    run->save();
    m_mgr.setCurrentRun(*run);
}

void RFMCommand::cmdRunStop(pugi::xml_node &node)
{
    RunInfo::RunNumber runNumber =
        node.attribute("runNumber").as_uint(RunInfo::InvalidRunNumber);
    if (runNumber == RunInfo::InvalidRunNumber)
        NTOF_THROW("Invalid runNumber", 1);

    RunInfo::Shared run = RFMFactory::instance().getRun(runNumber);
    if (!run)
        NTOF_THROW("Unknown run: " + std::to_string(runNumber), 1);

    if (!run->setStopDate(std::time(nullptr)))
        NTOF_THROW("Run already stopped at: " + std::to_string(run->stopDate()),
                   1);

    // stopped runs should be indexed
    run->createIndex();
    run->save();
}

void RFMCommand::cmdFileIgnore(pugi::xml_node &node)
{
    int32_t runNumber = node.attribute("runNumber").as_int(-1);
    if (runNumber < 0)
        NTOF_THROW("Invalid runNumber", 1);

    MergedFile::FileNumber fileNumber =
        node.attribute("fileNumber").as_uint(MergedFile::InvalidFileNumber);
    if (fileNumber == MergedFile::InvalidFileNumber)
        NTOF_THROW("Invalid fileNumber", 1);
    bool ignore = node.attribute("ignore").as_uint(1);

    RunInfo::Shared run = RFMFactory::instance().getRun(runNumber);
    if (!run)
        NTOF_THROW("Unknown run: " + std::to_string(runNumber), 1);

    MergedFile::Shared file;
    if (fileNumber == MergedFile::IndexFileNumber)
        file = run->index();
    else
        file = run->findFile(fileNumber);

    if (!file)
        NTOF_THROW(
            "Failed to ignore file: not found " + std::to_string(fileNumber), 1);

    if (ignore)
    {
        if (!file->setStatus(MergedFile::IGNORED))
            NTOF_THROW("Failed to ignore file: " +
                           MergedFile::toString(file->status()),
                       1);
    }
    else
    {
        if (!file->setStatus(run->isApproved() ?
                                 MergedFile::WAITING :
                                 MergedFile::WAITING_FOR_APPROVAL))
            NTOF_THROW("Failed to un-ignore file: " +
                           MergedFile::toString(file->status()),
                       1);
    }
    file->save();
}
