/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-03-03T10:07:55+01:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include "FileEvent.hpp"

#include "MergedFile.hpp"

#include "Storable.hxx"

using namespace ntof::rfm;

template class ntof::rfm::Storable<FileEvent>;

FileEvent::FileEvent(const MergedFile &file,
                     SequenceNumber seqNum,
                     EventNumber eventNumber,
                     std::size_t size,
                     std::size_t offset) :
    m_runNumber(file.runNumber()),
    m_fileNumber(file.fileNumber()),
    m_seqNum(seqNum),
    m_eventNumber(eventNumber),
    m_size(size),
    m_offset(offset)
{}

std::size_t FileEvent::offset() const
{
    std::lock_guard<std::mutex> lock(m_lock);
    return m_offset;
}

void FileEvent::setOffset(std::size_t offset)
{
    std::lock_guard<std::mutex> lock(m_lock);
    m_offset = offset;
}

std::size_t FileEvent::size() const
{
    std::lock_guard<std::mutex> lock(m_lock);
    return m_size;
}

void FileEvent::setSize(std::size_t size)
{
    std::lock_guard<std::mutex> lock(m_lock);
    m_size = size;
}
