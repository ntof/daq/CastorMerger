/*
 * RFMManager.hpp
 *
 *  Created on: Oct 8, 2015
 *      Author: mdonze
 */

#ifndef RFMMANAGER_HPP_
#define RFMMANAGER_HPP_

#include <condition_variable>
#include <cstdint>
#include <mutex>
#include <string>
#include <thread>
#include <vector>

#include <Worker.hpp>

#include "FileDeleter.hpp"
#include "RFMCommand.hpp"
#include "RFMMonitoring.hpp"
#include "data/MergedFile.hpp"
#include "data/RunInfo.hpp"

namespace ntof {
namespace rfm {

class MergerTask;
class IndexerTask;
class RFMDaqListener;

class RFMManager
{
public:
    typedef std::set<RunInfo::RunNumber> UpdatedRunsList;
    typedef ntof::utils::Worker Worker;
    typedef Worker::Task Task;

    RFMManager();
    virtual ~RFMManager();

    /**
     * Loads application configuration
     */
    void initialize();

    /**
     * @brief set currently active run
     * @param run [description]
     */
    void setCurrentRun(RunInfo &run);

    /**
     * Merge file on Archiver
     */
    void completeFile(MergedFile &file);

    /**
     * Merge index on Archiver
     */
    void mergeIndex(MergedFileIndex &index);

    /**
     * Called by the deleter task when an error occured
     * @param file Failed file
     * @param errorMsg Error message
     */
    void errorOccured(MergedFile &file, const std::string &errorMsg);

    /**
     * @brief run un current thread
     * @return false if manager is not initialized
     */
    bool run();

    /**
     * @brief start in a separate thread
     * @return false if manager is not initialized
     */
    bool start();

    /**
     * @brief stop existing threads
     * @return false if manager is not initialized
     */
    void stop();

    /**
     * @brief incoming validated event from EACS
     * @param runNumber Run number of the event
     * @param seqEventNumber Validated events sequence number
     * @param timingEventNumber Event number (Triggers count since start of run)
     * @param fileSize Size of the event in bytes
     * @return true if event was successfuly inserted
     */
    bool addEvent(int32_t runNumber,
                  int32_t seqEventNumber,
                  int32_t timingEventNumber,
                  uint64_t fileSize);

    /**
     * @brief incoming raw event from Daq
     * @param  runNumber         Run number of the event
     * @param  timingEventNumber event number
     * @param  fileSize          event file size
     * @return true if event was successfuly inserted
     */
    bool addDaqEvent(int32_t runNumber,
                     int32_t timingEventNumber,
                     uint64_t fileSize);

    FileDeleter *fileDeleter() const { return m_fileDeleter.get(); }
    RFMMonitoring *monitor() const { return m_mon.get(); }

protected:
    struct FileId
    {
        RunInfo::RunNumber runNumber;
        MergedFile::FileNumber fileNumber;
    };

    Worker m_worker;

    std::unique_ptr<FileDeleter> m_fileDeleter;
    std::unique_ptr<RFMCommand> m_cmd;
    std::unique_ptr<RFMMonitoring> m_mon;
    std::unique_ptr<RFMDaqListener> m_daqListener;

    UpdatedRunsList m_updates;

    std::mutex m_lock; //!< Mutex for protecting maps access
    std::condition_variable m_cond;
    std::unique_ptr<std::thread> m_thread;
    bool m_started;

    std::time_t m_next;

    /**
     * @brief sanity checks before starting the service
     */
    bool preStart();

    /**
     * @brief main routine
     */
    void thread_func();
    void triggerRetries();
    void expireRuns();
    void checkNextDate(std::time_t date);

    /**
     * @brief helper function for addEvent and addDaqEvent
     */
    bool addEvent(RunInfo &run,
                  int32_t seqEventNumber,
                  int32_t timingEventNumber,
                  uint64_t fileSize);
    /**
     * Gets if the file needs to be merged
     * @param file
     * @return
     */
    bool needsToMerge(const MergedFile &file) const;

    /**
     * @brief connect to the factory
     */
    void connect();

    void restoreFiles();
};

} // namespace rfm
} // namespace ntof

#endif
