/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-03-18T09:03:43+01:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#ifndef RFMCLI_HPP__
#define RFMCLI_HPP__

#include <cstdint>
#include <memory>
#include <vector>

#include <DIMData.h>
#include <DIMDataSetClient.h>
#include <DIMParamListClient.h>
#include <DIMSuperClient.h>

#include "RFMUtils.hpp"
#include "data/DaqInfo.hpp"
#include "data/MergedFile.hpp"
#include "data/RunInfo.hpp"

namespace ntof {
namespace rfm {

class RFMCli
{
public:
    typedef ntof::dim::DIMParamListClient::DataSignal ParamListSignal;
    typedef ntof::dim::DIMDataSetClient::DataSignal DataSetSignal;

    RFMCli();

    bool cmdStart(RunInfo::RunNumber runNumber,
                  const DaqInfo::CrateIdList &daqs,
                  const std::string &experiment = std::string(),
                  const bool *approve = nullptr,
                  const bool *standalone = nullptr);

    bool cmdStop(RunInfo::RunNumber runNumber);

    bool cmdEvent(RunInfo::RunNumber runNumber,
                  FileEvent::SequenceNumber seq,
                  FileEvent::EventNumber event,
                  std::size_t size = 0);

    bool cmdFileIgnore(RunInfo::RunNumber runNumber,
                       MergedFile::FileNumber fileNumber,
                       bool ignore);

    bool setRunExpiry(RunInfo::RunNumber runNumber, std::time_t expiry);
    bool setRunExperiment(RunInfo::RunNumber runNumber,
                          const std::string &experiment);
    bool setRunApproved(RunInfo::RunNumber runNumber);

    void listen();

    std::vector<ntof::dim::DIMData> getRunsInfo() const;
    std::vector<ntof::dim::DIMData> getCurrentFiles() const;

    /**
     * rpc call to get files info for a specific run
     */
    std::vector<ntof::dim::DIMData> getFiles(RunInfo::RunNumber run) const;

    ParamListSignal runsSignal;
    ParamListSignal currentSignal;
    DataSetSignal filesSignal;

protected:
    ntof::dim::DIMSuperClient m_cmd;
    std::unique_ptr<ntof::dim::DIMParamListClient> m_runsCli;
    std::unique_ptr<ntof::dim::DIMParamListClient> m_currentCli;
    std::unique_ptr<ntof::dim::DIMDataSetClient> m_filesCli;
};

} // namespace rfm
} // namespace ntof

#endif
