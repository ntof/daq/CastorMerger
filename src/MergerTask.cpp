/*
 * MergerTask.cpp
 *
 *  Created on: Oct 20, 2015
 *      Author: mdonze
 */
#include "MergerTask.h"

#include <fstream>
#include <iostream>
#include <sstream>

#include <easylogging++.h>

#include "Archiver.hpp"
#include "Config.h"
#include "FileUtils.hpp"
#include "Paths.hpp"
#include "RFMException.hpp"
#include "RFMFactory.hpp"
#include "RawHeaders.h"

#include <xrootd/XrdCl/XrdClFile.hh>
#include <xrootd/XrdCl/XrdClFileSystem.hh>

namespace bfs = boost::filesystem;
using namespace ntof::rfm;

uint64_t MergerTask::bufferSize = 1 * 1024 * 1024;

/**
 * Constructor
 * It will open the file descriptor
 * @param fileName Name of the file on the file system
 */
RawFile::RawFile(const std::string &fileName) : start_(0), end_(0), file_(NULL)
{
    VLOG(1) << "Constructing RawFile object with file name : " << fileName;
    file_ = new std::ifstream(fileName.c_str(),
                              std::ios::in | std::ios::binary | std::ios::ate);
    if (file_->is_open())
    {
        // Gets complete file size
        end_ = file_->tellg();
        LOG(TRACE) << "File " << fileName << " size is " << end_ << " bytes";
        // Checks if the file is enough long to contain a SKIP header
        if (end_ >= sizeof(SKIP))
        {
            // Move to beginning of file
            file_->seekg(0, std::ios::beg);
            // Use smart pointer to avoid memory leaks
            std::vector<char> dataPtr;
            dataPtr.reserve(sizeof(SKIP));
            char *data = dataPtr.data();
            file_->read(data, sizeof(SKIP));
            // Test if SKIP header is found at beginning of file
            SKIP *skip = (SKIP *) data;
            std::string headerTitle(skip->header.title, 4);
            if (headerTitle != "SKIP")
            {
                std::ostringstream oss;
                oss << "Raw file " << fileName
                    << " not valid, expecting a SKIP header at beginning!";
                LOG(INFO) << "Warning : " << oss.str();
                // TODO: Maybe throw error/warning
            }
            else
            {
                start_ = skip->start;
                end_ = skip->end;
            }
        }
        LOG(TRACE) << "File " << fileName << " starts at " << start_
                   << ", ends at " << end_;
    }
    else
    {
        std::ostringstream oss;
        oss << "Unable to open file " << fileName;
        LOG(ERROR) << oss.str();
        throw RFMException(oss.str());
    }
}

RawFile::~RawFile()
{
    VLOG(1) << "Destroying RawFile object";
    closeFile();
    delete file_;
}

/**
 * Gets the event size stored into this raw file
 * @return
 */
uint64_t RawFile::getEventSize()
{
    return end_ - start_;
}

/**
 * Close file if open
 */
void RawFile::closeFile()
{
    if ((file_ != NULL) && file_->is_open())
    {
        file_->close();
    }
}

/**
 * Gets the event start offset
 * @return
 */
uint64_t RawFile::getEventStart()
{
    return start_;
}

/**
 * Gets the event end offset
 * @return
 */
uint64_t RawFile::getEventEnd()
{
    return end_;
}

/**
 * Gets pointer to file
 * @return
 */
std::ifstream *RawFile::getFile()
{
    return file_;
}

/**
 * Sets the buffer size
 * @param size
 */
void MergerTask::setBufferSize(uint64_t size)
{
    if (bufferSize > 0)
    {
        bufferSize = size;
    }
    else
    {
        throw NTOFException("Buffer size cannot be 0!", __FILE__, __LINE__);
    }
}

MergerTask::MergerTask(MergedFile &file) :
    m_run(RFMFactory::instance().getRun(file.runNumber())),
    m_file(file.shared()),
    m_offset(0)
{
    VLOG(1)
        << "Constructing MergerTask with CASTORFile : " << m_file->fileName();
}

MergerTask::~MergerTask()
{
    VLOG(1) << "Destroying MergerTask with CASTORFile : " << m_file->fileName();
}

/**
 * Renames an existing file on CASTOR
 * @param resFile
 */

void MergerTask::renameExistingCASTORFile(const bfs::path &resFile)
{
    int i = 0;
    bfs::path oldFile = resFile;
    oldFile += ".old";
    while (Archiver::instance().getInfo(oldFile))
    {
        ++i;
        oldFile = resFile;
        oldFile += ".old" + std::to_string(i);
    }
    Archiver::instance().renameFile(resFile, oldFile);
}

void MergerTask::run()
{
    std::ostringstream errMsg;
    try
    {
        if (!m_run || !m_file)
            throw NTOFException("can't merge null file", __FILE__, __LINE__);

        LOG(INFO) << "[MergerTask] merging: " << m_file->fileName();

        MergedFile::EventsList events = m_file->getEvents();
        if (events.empty())
        {
            LOG(WARNING) << "[MergerTask]: no events on: " << m_file->fileName()
                         << " -> IGNORED";
            m_file->setStatus(MergedFile::IGNORED);
            m_file->save();
            return;
        }

        bfs::path resFile = Paths::getRemoteFilePath(*m_run, *m_file);
        // Create the CASTOR folder (and parent sub folders)
        Archiver::instance().createDir(resFile.parent_path());

        // Query CASTOR file information
        Archiver::FileInfo info = Archiver::instance().getInfo(resFile);
        if (info)
        {
            // File exists on CASTOR
            if (Config::instance().getReplaceStrategy() ==
                Config::ReplaceStrategy::DELETE)
            {
                LOG(INFO) << "File " << resFile.string()
                          << " already exists on archiver, deleting it...";
                Archiver::instance().deleteFile(resFile);
            }
            else if (Config::instance().getReplaceStrategy() ==
                     Config::ReplaceStrategy::RENAME)
            {
                if (info.fileSize == 0)
                {
                    LOG(INFO) << "File " << resFile
                              << " already exists on archiver with empty size, "
                                 "deleting it...";
                    Archiver::instance().deleteFile(resFile);
                }
                else
                {
                    LOG(INFO) << "File " << resFile
                              << " already exists on archiver, renaming it...";
                    renameExistingCASTORFile(resFile);
                }
            }
        }
        LOG(INFO) << "Merging " << events.size() << " events to "
                  << m_file->fileName();

        // Update file status (merging will start now)
        m_file->setStatus(MergedFile::TRANSFERRING);
        m_file->setTransferred(0);
        m_file->save();

        mergeToCASTOR(resFile);

        // Merging finished, update the database
        m_file->setStatus(MergedFile::COPIED);
        m_file->setSize(m_offset);
        m_file->save();

        // Task completed
        taskSignal(std::string());
        return;
    }
    catch (const RFMException &ex)
    {
        errMsg << "RFMException (" << m_file->fileName() << ") : " << ex.what();
    }
    catch (const NTOFException &ex)
    {
        errMsg << "NTOFException ("
               << (m_file ? m_file->fileName() : std::string("null"))
               << ") : " << ex.getMessage();
    }
    catch (const std::exception &ex)
    {
        errMsg << "Exception (" << m_file->fileName() << ") : " << ex.what();
    }
    catch (...)
    {
        errMsg << "Unknown exception on file : " << m_file->fileName();
    }
    try
    {
        // Reset status to WAITING
        m_file->setStatus(MergedFile::WAITING);
        m_file->save();
    }
    catch (...)
    {}

    std::string err = errMsg.str();
    err = err.empty() ? std::string("unknown error") : err;
    LOG(ERROR) << "[MergerTask]:" << err;
    taskSignal(err);
}

void MergerTask::mergeToCASTOR(const bfs::path &castorPath)
{
    LOG(DEBUG) << "Merging data to CASTOR file : " << castorPath.string();
    RFMFactory::DaqInfoList daqs(
        RFMFactory::instance().getDaqInfoList(m_run->getDaqs()));

    Archiver::File::Shared file = Archiver::instance().open(castorPath);

    m_offset = 0; // We start at the beginning of the file
    // First, add RCTR and MODH
    appendRunHeaders(*file);

    LOG(TRACE) << "Going trough events to be written to " << m_file->fileName();
    // Go trough events to complete the file

    MergedFile::EventsList events = m_file->getEvents();
    for (FileEvent::Shared &evt : events)
    {
        // Open all raw files for computing sizes
        std::vector<RawFile::Shared> rawFiles;
        rawFiles.reserve(daqs.size());

        for (DaqInfo::Shared &daq : daqs)
        {
            RawFile::Shared raw(new RawFile(
                Paths::getRawFilePath(*daq, *m_run, *evt).string()));
            rawFiles.push_back(raw);
        }

        // Add EVEH and ADDH to file
        appendEventHeaders(*file, *evt, rawFiles);

        for (RawFile::Shared &rawFile : rawFiles)
        {
            appendRawData(*file, *rawFile);
        }
    }

    // Sync and close the CASTOR file
    LOG(TRACE) << "Syncing and closing file " << castorPath;
    file.reset();
}

/**
 * Appends RCTR and MODH to the CASTOR file
 * @param cFile
 */
void MergerTask::appendRunHeaders(Archiver::File &out)
{
    LOG(TRACE)
        << "Appending RCTR and MODH to CASTOR file " << m_file->fileName();

    const std::string filePath = Paths::getRunFilePath(*m_run).string();

    LOG(TRACE) << "Opening local file " << filePath;
    // As the .run file contains only RCTR and MODH, we can open whole file in
    // memory
    std::ifstream file(filePath.c_str(),
                       std::ios::in | std::ios::binary | std::ios::ate);
    if (file.is_open())
    {
        std::streampos size = file.tellg();
        // Use smart pointer to avoid memory leaks
        std::vector<char> dataPtr;
        dataPtr.reserve(size);
        char *data = dataPtr.data();
        file.seekg(0, std::ios::beg);
        file.read(data, size);
        file.close();
        LOG(TRACE)
            << "Local file " << filePath << " read OK with size " << size;
        // Change the RCTR header with the good extension number
        RCTR *rctr = (RCTR *) data;
        std::string headerTitle(rctr->header.title, 4);
        if (headerTitle != "RCTR")
        {
            throw RFMException(std::string("RCTR missing in ") + filePath);
        }
        LOG(TRACE)
            << "Updating RCTR event number with " << m_file->fileNumber();
        rctr->fileNumber = m_file->fileNumber();
        // MODH doesn't need extra-modifications
        // Write data to CASTOR file
        writeData(out, size, data);
    }
    else
    {
        throw RFMException(std::string("Unable to open file: ") + filePath);
    }
}

void MergerTask::appendEventHeaders(Archiver::File &out,
                                    FileEvent &evt,
                                    const MergerTask::RawFilesList &rawFiles)
{
    LOG(TRACE) << "Appending EVEH and ADDH to CASTOR file "
               << m_file->fileName() << " timing event #" << evt.eventNumber();

    std::string filePath = Paths::getEventFilePath(*m_run, evt).string();
    LOG(TRACE) << "Opening local file " << filePath;
    // As the .event file contains only EVEH and ADDH, we can open whole file in
    // memory
    std::ifstream file(filePath.c_str(),
                       std::ios::in | std::ios::binary | std::ios::ate);
    if (file.is_open())
    {
        std::streampos size = file.tellg();
        // Use smart pointer to avoid memory leaks
        std::vector<char> dataPtr;
        dataPtr.reserve(size);
        char *data = dataPtr.data();
        file.seekg(0, std::ios::beg);
        file.read(data, size);
        file.close();
        LOG(TRACE)
            << "Local file " << filePath << " read OK with size " << size;

        // Compute size of event, first in byte
        uint32_t evtSize = size;
        for (const RawFile::Shared &rawFile : rawFiles)
        {
            evtSize += rawFile->getEventSize();
        }

        // Convert size of event to words
        evtSize /= sizeof(int32_t);

        // Change the EVEH header with the good extension number
        EVEH *eveh = (EVEH *) data;
        std::string headerTitle(eveh->header.title, 4);
        if (headerTitle != "EVEH")
        {
            throw RFMException(std::string("EVEH missing in ") + filePath);
        }
        LOG(TRACE) << "Updating event number to " << evt.sequenceNumber()
                   << " for CASTOR file " << m_file->fileName()
                   << " timing event #" << evt.eventNumber();
        eveh->evtNumber = evt.sequenceNumber();

        LOG(TRACE)
            << "Updating event size to " << evtSize << " for CASTOR file "
            << m_file->fileName() << " timing event #" << evt.eventNumber();
        // Sets the size of event
        eveh->evtWCount = evtSize;

        evt.setOffset(m_offset);
        evt.save();
        writeData(out, size, data);
    }
    else
    {
        throw RFMException(std::string("Unable to open file: ") + filePath);
    }
}

void MergerTask::appendRawData(Archiver::File &out, RawFile &rawFile)
{
    // Only copy data if size is positive
    if (rawFile.getEventEnd() > 0)
    {
        std::ifstream *file = rawFile.getFile();

        // Seek to beginning of start
        rawFile.getFile()->seekg(rawFile.getEventStart(), std::ios::beg);

        std::vector<char> buffer;
        buffer.reserve(bufferSize);
        char *data = buffer.data();
        while (true)
        {
            std::streampos pos = file->tellg();
            uint64_t toRead = rawFile.getEventEnd();
            if (static_cast<uint64_t>(pos) >= toRead)
            {
                break;
            }

            toRead -= pos;
            if (toRead > bufferSize)
            {
                toRead = bufferSize;
            }
            file->read(data, toRead);
            // Write data to CASTOR file
            writeData(out, file->gcount(), data);
        }
    }
    rawFile.closeFile();
}

void MergerTask::writeData(Archiver::File &archive,
                           uint32_t size,
                           const void *buffer)
{
    archive.write(m_offset, size, buffer);
    m_offset += size;
    m_file->setTransferred(m_offset);
}
