
string(TOLOWER "${CMAKE_PROJECT_NAME}" PACKAGE_NAME)
set(CPACK_PACKAGE_NAME "${PACKAGE_NAME}")
set(CPACK_PACKAGE_VENDOR "CERN")
set(CPACK_PACKAGE_INSTALL_REGISTRY_KEY "${CMAKE_PROJECT_NAME}")
set(CPACK_RESOURCE_FILE_LICENSE "${CMAKE_SOURCE_DIR}/COPYING")
set(CPACK_PACKAGE_VERSION_MAJOR "${VERSION_MAJOR}")
set(CPACK_PACKAGE_VERSION_MINOR "${VERSION_MINOR}")
set(CPACK_PACKAGE_VERSION_PATCH "${VERSION_PATCH}")

set(CPACK_PACKAGE_CONTACT "Sylvain Fargier <sylvain.fargier@cern.ch>")

# Generate debug package
set(CPACK_RPM_DEBUGINFO_PACKAGE ON)

if(CMAKE_SYSTEM_PROCESSOR STREQUAL "x86_64")
  set(REQ_SUF "(x86-64)")
endif()

set(REQS "glibc${REQ_SUF}, libgcc${REQ_SUF}, libstdc++${REQ_SUF}")
set(REQS "${REQS}, xrootd-client-libs${REQ_SUF}")
set(REQS "${REQS}, castor-lib${REQ_SUF}")
set(REQS "${REQS}, boost${REQ_SUF}")
set(REQS "${REQS}, oracle-instantclient19.5-basic${REQ_SUF}")
set(CPACK_RPM_PACKAGE_REQUIRES "${REQS}")
set(CPACK_RPM_PACKAGE_AUTOREQ 0)
set(CPACK_PROJECT_CONFIG_FILE "${CMAKE_SOURCE_DIR}/cmake/PackConfig.cmake")

add_custom_target(rpm-package COMMAND ${CMAKE_CPACK_COMMAND} -G RPM)

include(CPack)
